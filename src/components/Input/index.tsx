import React from 'react'
// Styles
import styles from './styles.module.css';

type Props = {
  label: string,
  inputProps: React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>
}

const Input = ({
  label,
  inputProps
}: Props) => {
  return (
    <div className={styles.container}>
      <div className={styles.labelContainer}>
        <label>{label}</label>
      </div>
      <div>
        <input type="text" {...inputProps} />
      </div>
    </div>
  )
}

export default Input