# Next.js + Jest

This example shows how to configure Jest to work with Next.js.

This includes Next.js' built-in support for Global CSS, CSS Modules and TypeScript.

## How to Use

In your terminal, run the following command:

```bash
npm run dev
```

## Use with docker

```bash
docker compose up -d
docker compose exec app npm install
docker compose exec app npm run dev
```

## Run Jest Tests

```bash
npm run test
```