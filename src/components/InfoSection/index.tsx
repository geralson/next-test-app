import React from 'react'
import Input from '../Input'
import TextArea from '../TextArea';
import styles from './styles.module.css';

type Props = {}

const InfoSection = (props: Props) => {
  return (
    <section className={styles.container}>
      <div>
        <Input
          label='Nombre'
          inputProps={{
            type: 'text',
          }}
        />
      </div>
      <div>
        <TextArea
          label='Descripción'
          textAreaProps={{
          }}
        />
      </div>
    </section>
  )
}

export default InfoSection