import Header from '@/components/Header'
import { ReactNode } from 'react'
// Styles
import styles from './styles.module.css';

type Props = {
  children: ReactNode
}

const GeneralLayaut = ({ children }: Props) => {
  return (
    <div>
      <Header />
      <main className={styles.main}>
        {children}
      </main>
    </div>
  )
}

export default GeneralLayaut