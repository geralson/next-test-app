import React from 'react'
// Styles
import styles from './styles.module.css';

type Props = {
  label: string,
  textAreaProps?: React.DetailedHTMLProps<React.TextareaHTMLAttributes<HTMLTextAreaElement>, HTMLTextAreaElement>
}

const TextArea = ({
  label,
  textAreaProps
}: Props) => {
  return (
    <div className={styles.container}>
      <div className={styles.labelContainer}>
        <label>{label}</label>
      </div>
      <div>
        <textarea {...textAreaProps} />
      </div>
    </div>
  )
}

export default TextArea