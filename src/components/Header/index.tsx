import React from 'react'
// Styles
import styles from './styles.module.css';

type Props = {}

const Header = (props: Props) => {
  return (
    <header className={styles.header}>
      <div className={styles.container}>
        <div>
          <h1>Nueva tabla</h1>
        </div>
        <div>
          <button className='btn btn-primary'>
            GUARDAR
          </button>
        </div>
      </div>
    </header>
  )
}

export default Header