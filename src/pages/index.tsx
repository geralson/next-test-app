import InfoSection from '@/components/InfoSection'

import GeneralLayaut from 'src/layauts/GeneralLayaut'

export default function Home() {
  return (
    <GeneralLayaut>
      <div className='pt-4'>
        <InfoSection />
      </div>
    </GeneralLayaut>
  )
}
